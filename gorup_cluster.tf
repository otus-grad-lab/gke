variable "gitlab_token" {
    type = string
    description = "Provide a GitLab access token with admin rights to the GitLab group set as the `gitlab_group` variable"
}

variable "gitlab_mgmt_project_id" {
  type = string
  description = " The ID of the management project for the cluster"
  default = "28522429"
}

provider "gitlab" {
    token = var.gitlab_token
}

data "gitlab_group" "iac-group" {
  # full_path = var.gitlab_group
  full_path = "otus-grad-lab"
}

resource "gitlab_group_cluster" "gke_cluster" {
  group                 = data.gitlab_group.iac-group.id
  name                  = google_container_cluster.primary.name
  domain                = "nip.io"
  environment_scope     = "*"
  kubernetes_api_url    = "https://${google_container_cluster.primary.endpoint}"
  kubernetes_token      = data.kubernetes_secret.gitlab-admin-token.data.token
  kubernetes_ca_cert    = trimspace(base64decode(google_container_cluster.primary.master_auth.0.cluster_ca_certificate))
  management_project_id = var.gitlab_mgmt_project_id
}

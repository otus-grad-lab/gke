<H3>Содержание</H3>
<p>
1. [Intro](#intro)<br>
2. [Требования к проектной работе](#conditions)<br>
3. [Общее описание решения](#general)<br>
4. [Последовательность действий](#sequence)<br>
</p>
<a name="intro"></a><H3>Intro</H3>
<p>
Данный проект в Gitlab, а также вся группа otus-grad-lab являются проектной работой по курсу «Инфраструктурная платформа на основе Kubernetes». Проект содержит описание инфраструктуры в виде кода (Infrastructure as Code, IaC) по созданию Kubernetes-кластера в облаке Google с последующим развёртыванием в кластере инфраструктурных сервисов и демонстрационного микросервисного проекта [Online Boutique (Hipster Shop)](https://github.com/GoogleCloudPlatform/microservices-demo).
</p>
<a name="conditions"></a><H3>Требования к проектной работе</H3>
<p>
<details>
	<summary><li>1. Функционал</summary>
		<ol>
			<li>Kubernetes (managed или self-hosted)</li>
			<li>Мониторинг кластера и приложения с алертами и дашбордами</li>
			<li>Централизованное логирование для кластера и приложений</li>
			<li>CI/CD пайплайн для приложения (Gitlab CI, Github Actions, etc)</li>
		</ol>
</details>
<details>
	<summary><li>2. Оформление</summary>
		<ol>
			<li>Автоматизация создания и настройки кластера</li>
			<li>Развертывание и настройки сервисов платформы</li>
			<li>Настройки мониторинга, алерты, дашборды</li>
			<li>CI/CD пайплайн для приложения описан кодом</li>
		</ol>
</details>
<details>
	<summary>3. Доступы</summary>
		<ol>
			<li>Публично доступный репозиторий с кодом и пайплайном</li>
			<li>Публично доступный ingress кластера (белый IP, домен)</li>
		</ol>
</details>
<details>
	<summary>4. Документация</summary>
		<ol>
			<li>README с описанием решения, changelog</li>
			<li>Шаги развертывания и настройки компонентов</li>
			<li>Описание интеграции с CI/CD-сервисом</li>
			<li>Подход к организации мониторинга и логирования</li>
			<li>Дополнительная информация про вашу платформу</li>
		</ol>
</details>
<details>
	<summary>5. Приложение</summary>
		<ol>
			<li>Приложение на ваш выбор</li>
			<li>Opensource</li>
			<li>Подходящаяя лицензия (MIT, Apache, etc)</li>
			<li>Доступно по HTTP</li>
			<li>Минимум три развертываемых компонента</li>
		</ol>
</details>
</p>
<a name="general"></a><H3>Общее описание решения</H3>
<p>
<ul>
<li>Для создания инфраструктурной платформы был выбран сервис публичных репозиториев Gitlab, а для реализации CI/CD-пайплайна используется код, размещенный в файлах .gitlab-ci.yaml.</li>
<li>Kubernetes-кластер разворачивается в публичном облаке Google, таким образом, в проектной работе используется используется публичное облако Google и GKE.</li>
<li>Для создания GKE-кластера используется Terraform, который при вызывается в ходе выполнения пайплайна Gitlab и создаёт GKE-кластер в соответствии с предопределёнными настройками.</li>
<li>Приложения и сервисы располагаются в отдельном репозитории, содержащем манифесты утилиты helmfile, и выкатываются отдельным пайплайном Gitlab.</li>
<li>В качестве миркосервисного приложения выбран проект [Online Boutique](https://github.com/GoogleCloudPlatform/microservices-demo), состоящий из нескольких микросервисов и реализующий интернет-магазин.</li>
<li>Кроме того в кластер устанавливаются инфраструктурные сервисы такие как – ingress-controller, elastic-stack, prometheus, grafana и т.д.</li>
</p>
<a name="sequence"></a><H3>Последовательность действий</H3>
<ol>
<p>
<li>Создать группу в Gitlab. Для нашего проекта была создана группа [otus-grad-lab](https://gitlab.com/otus-grad-lab/)</li>
</p>
<p>
<li>Назначить Gitlab-пользователю права maintainer на вновь созданную группу [otus-grad-lab](https://gitlab.com/otus-grad-lab/). Для этого пользователя сгенерировать в настройках Gitlab токен с правами на доступ к api.</li>
</p>
<p>
<li>[Создать](https://cloud.google.com/docs/authentication/getting-started) сервис-аккаунт в Google cloud, от имени данного сервис аккаунта будет в дальнейшем выполняться создание GKE-кластера.</li>
</p>
<p>
<li>Добавить в группу [otus-grad-lab](https://gitlab.com/otus-grad-lab/) репозиторий с манифестами Terraform для создания кластера – [gke](https://gitlab.com/otus-grad-lab/gke/).
Репозиторий [gke](https://gitlab.com/otus-grad-lab/gke/) содержит следующие terraform-манифесты:
<ul>
<li><i>provider.tf</i> – переменные и настройки провайдера для подключения к проекту в Google Cloud;</li>
<li><i>gke.tf</i> – описание ресурсов (конфигурация кластера), которые должны быть созданы в GKE;</li>
<li><i>gitlab-admin.tf</i> – описание провайдера Kubernetes а так же ресурсов необходимых для создания роли, под которой будет производиться упарвление кластером из gitlab-ci;</li>
<li><i>gorup_cluster.tf</i> – содержит описание провайдера Gitlab и необходимые для создание Kubernetes-кластера в разделе Infrastructure проекта Gitlab;</li>
<li><i>outputs.tf</i> – содержит описание возвращаемых значений после создания кластера;</li>
<li><i>versions.tf</i> – содержит описание требований к версиям используемых провайдеров.</li>
</ul>
</p>
<p>
<li>Для успешного создания gke-кластера понадобится определить несколько переменных уровня проекта [gke](https://gitlab.com/otus-grad-lab/gke/) или группы [otus-grad-lab](https://gitlab.com/otus-grad-lab/) содержащих токен для подключения к Gitlab, учетные данные и параметры проекта для подключения к Google cloud.</li>
</p>
<p>
<li>Добавить в группу [otus-grad-lab](https://gitlab.com/otus-grad-lab/) репозиторий с манифестами для деплоя сервисов (рабочей нагрузки) в созданный GKE-кластер – [apps/cluster-management](https://gitlab.com/otus-grad-lab/apps/cluster-management). Репозиторий создаётся из шаблона Gitlab, при этом необходимо выбрать шаблон [Cluster Management Project Template](https://docs.gitlab.com/ee/user/clusters/management_project_template.html)</li>
</p>
<p>
<li>В репозитории [apps/cluster-management](https://gitlab.com/otus-grad-lab/apps/cluster-management) создать Environment и назвать его Production. Это необходимо для работы Cluster management</li>
</p>
<p>
<li>В репозитории [gke](https://gitlab.com/otus-grad-lab/gke/) разместить файл .gitlab-ci.yaml, который будет содержать директивы (стадии и шаги) последовательности действий для создания GKE-кластера в облаке Google:
<ul>
<li><i>init</i> – на данном шаге на gitlab-раннере выполняется terraform init для инициализации рабочего каталога с манифестами terraform;</li>
<li><i>validate</i> – на данном шаге выполняется terraform validate – проверка манифестов на корректность;</li>
<li><i>build</i> – на данном шаге выполняется terraform plan;</li>
<li><i>deploy</i> – на данном шаге выполняется создание GKE-кластера;</li>
<li><i>deploy-apps</i> – ручной запуск; на данном этапе стартует пайплайн проекта [apps/cluster-management](https://gitlab.com/otus-grad-lab/apps/cluster-management) при выполнении которого устанавливаются инфраструктурные сервисы и демонстрационного микросервисного проекта Online Boutique;</li>
<li><i>cleanup (destroy)</i> – удаление ранее созданного кластера.</li>
</ul>
</li>
</p>
<p>
<li>Запускаем пайплайн в проекте [gke](https://gitlab.com/otus-grad-lab/gke/). В результате успешного запуска пайплайна в Google Cloud должен быть создан Kubernetes-кластер, который также будет привязан к группе в Gitlab. Кроме того, проект [apps/cluster-management](https://gitlab.com/otus-grad-lab/apps/cluster-management) в ходе выполнения пайплайна будет привязан как проект Cluster management в настройках нашего кластера GKE в Gitlab</li>
</p>
<p>
<li>Для инициализации рабочей нагрузки необходимо в ручном режиме запустить выполнение шага пайплайна <i>deploy-apps</i>. На данном шаге производится установка сервисов с применением утилиты helmfile.</i>
</p>
<p>
<li>
При успешном выполнении данного шага будет создан сервисный пайплайн из проекта [apps/cluster-management](https://gitlab.com/otus-grad-lab/apps/cluster-management), после чего автоматически запустится на выполнение шаг сервисного пайплайна <i>init-app</i>.
</li>
</p>
<p>
<li>
	По окончанию работы пайплайна на шаге <i>init-app</i> в кластер GKE будут установлены следующие инфраструктурные сервисы, а также сервисы демо-проекта:
	<ul>
		<li>kube-prometheus-stack (prometheus-operator, prometheus, grafana, node-exporter, alertmanager);</li>
		<li>ingress-nginx;</li>
		<li>redis;</li>
		<li>elastic-stack (filebeat, elasticsearch кластер, kibana);</li>
		<li>сервисы демо-проекта [Online Boutique](https://github.com/GoogleCloudPlatform/microservices-demo).</li>
	</ul>
	Инфраструктурные сервисы устанавливаются в неймспейс <i>gitlab-managed-apps</i>, а микросервисы демо-проекта в неймспейс <i>applications</i>.
</li>
</p>
<p>
<li>
Кроме того, в выводе результатов пайплайна, в последних строчках логов на шаге <i>init-app</i> будут отражены все ingress-сервисы, доступные для использования.
</li>
</p>
<p>
<li>
Кроме шага первичной инициализации сервисный пайплайн проекта [apps/cluster-management](https://gitlab.com/otus-grad-lab/apps/cluster-management) содержит два дополнительных шага для каждого из сервисов демо-проекта: <i>Fake-build-app</i> и <i>Deploy-app</i>
<ul>
<li>шаг <i>Fake-build-app</i> – иммитирует сборку образа после изменений в коде, а так же сборку новой версии сервиса. 
<li>шаг <i>Deploy-app</i> – непосредственно производит обновление сервиса в GKE-кластере с версией образа, созданного на шаге <i>Fake-build-app</i>.
</ul>
</li>
</p>
<p>
<li>
	Дополнительно:
	<ul>
	<li>для демонстрации работы с prometheus-operator, а также автоматического поиска сервисов Прометеем, в репозиторий проекта для сервиса демо-проекта adservice добавлен манифест Service monitor;</li>
	<li>добавлен кастомный [дашборд](https://gitlab.com/otus-grad-lab/apps/cluster-management/-/blob/master/applications/prometheus-operator/prom-operator-chart/templates/grafana/dashboards-1.14/hipstershop-db.yaml) Grafana, который, как и другие, дашборды добавляется в Графану автоматически при деплое и показывает Opencensus-метрики приложений из состава демо-проекта;</li>
	<li>кроме того, для демонстрации работы с alerter добавлены кастомные [алерты](https://gitlab.com/otus-grad-lab/apps/cluster-management/-/blob/master/applications/prometheus-operator/prom-operator-chart/templates/prometheus/rules-1.14/hipster.yaml).</li>
	</ul>
</li>
</p>
</ol>

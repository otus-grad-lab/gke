variable "project_name" {
   default = "celestial-brand-321222" # gcloud projects list
}

variable "region" {
  default = "us-central1-a" # gcloud compute zones list
}

provider "google" {
   credentials = "${file("./creds/serviceaccount.json")}"
   project = "${var.project_name}"
   region = "${var.region}"
}

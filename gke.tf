resource "google_container_cluster" "primary" {
  name                     = "otus-test"
  location                 = "us-central1-a"
  remove_default_node_pool = true
  initial_node_count       = 1
  min_master_version       = "1.19"
  description              = "test"
}

resource "google_container_node_pool" "primary_preemptible_nodes" {
  name       = "otus-node-pool-1"
  cluster    = google_container_cluster.primary.name
  location   = "us-central1-a"
  node_count = 4

  node_config {
    preemptible  = true
    machine_type = "n1-standard-2"

    metadata = {
      disable-legacy-endpoints = "true"
    }

    oauth_scopes = [
      "https://www.googleapis.com/auth/logging.write",
      "https://www.googleapis.com/auth/monitoring",
    ]
  }
}

